using System;

namespace Hask.GraphVisualization.GraphVizUtil
{
    public class GraphVizConsoleArgsBuilder
    {
        public string InputFilePathParam { get; set; }
        
        public string OutputFilePathParam { get; set; }

        public GraphVizOutputFormats OutputFormatParam { get; set; } = GraphVizOutputFormats.Jpeg;

        public string BuildArgString()
        {
            var outputFormatstr = "-T";
            switch (OutputFormatParam)
            {
                case GraphVizOutputFormats.Jpeg:
                    outputFormatstr += "jpeg";
                    break;
                case GraphVizOutputFormats.Json:
                    outputFormatstr += "json";
                    break;
                case GraphVizOutputFormats.Svg:
                    outputFormatstr += "svg";
                    break;
                default:
                    throw new NotImplementedException($"Output type {OutputFormatParam} is not implemented");
            }

            return $"{outputFormatstr} {InputFilePathParam} -o {OutputFilePathParam}";
        }
    }
}