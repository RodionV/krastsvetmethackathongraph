namespace Hask.GraphVisualization.GraphVizUtil
{
    public enum GraphVizOutputFormats
    {
        Jpeg,
        Json,
        Svg
    }
}