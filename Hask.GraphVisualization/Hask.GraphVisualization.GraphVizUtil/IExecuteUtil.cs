using System.Threading.Tasks;

namespace Hask.GraphVisualization.GraphVizUtil
{
    public interface IExecuteUtil
    {
        Task Execute(string path, string args);
    }
}