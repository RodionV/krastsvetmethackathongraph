using System.Threading.Tasks;

namespace Hask.GraphVisualization.GraphVizUtil
{
    public class GraphVizUtil
    {
        private readonly GraphVizOptions _options;
        private readonly IExecuteUtil _executeUtil;

        public GraphVizUtil(GraphVizOptions options, IExecuteUtil executeUtil)
        {
            _options = options;
            _executeUtil = executeUtil;
        }

        public async Task GenerateGraph(GenerateGraphOptions options)
        {
            var argsBuider = new GraphVizConsoleArgsBuilder();
            argsBuider.InputFilePathParam = options.InputFilePath;
            argsBuider.OutputFilePathParam = options.OutputFilePath;
            argsBuider.OutputFormatParam = options.Format;
            var argsString = argsBuider.BuildArgString();

            await _executeUtil.Execute(_options.PathToUtil, argsString);
        }
    }
}