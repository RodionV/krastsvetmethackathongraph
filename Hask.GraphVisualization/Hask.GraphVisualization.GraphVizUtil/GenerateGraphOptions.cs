namespace Hask.GraphVisualization.GraphVizUtil
{
    public class GenerateGraphOptions
    {
        public string OutputFilePath { get; set; }
        
        public string InputFilePath { get; set; }
        
        public GraphVizOutputFormats Format { get; set; }
    }
}