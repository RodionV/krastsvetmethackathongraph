﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hask.GraphVisualization.GraphVizUtil;
using Hask.GraphVisualization.WebApi.Helpers.ExecuteUtil;
using Hask.GraphVisualization.WebApi.Helpers.GraphDeserializer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hask.GraphVisualization.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddScoped<IExecuteUtil, ExecuteUtil>();
            services.AddSingleton<GraphVizOptions>(new GraphVizOptions()
            {
                PathToUtil = $"C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe"
            });
            services.AddScoped<GraphVizUtil.GraphVizUtil>();
            services.AddScoped<IGraphDeserializer, JsonGraphDeserializer>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}