using Microsoft.AspNetCore.Mvc;

namespace Hask.GraphVisualization.WebApi.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
    }
}