using Hask.GraphVisualization.Core.DTO;
using Newtonsoft.Json;

namespace Web.Core.Helpers.GraphDeserializer
{
    public class JsonGraphDeserializer : IGraphDeserializer
    {
        public GraphModel Deserialize(string serializedGraph)
        {
            var graph = JsonConvert.DeserializeObject<GraphModel>(serializedGraph);

            return graph;
        }
    }
}