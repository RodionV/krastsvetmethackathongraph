using Hask.GraphVisualization.Core.DTO;

namespace Web.Core.Helpers.GraphDeserializer
{
    public interface IGraphDeserializer
    {
        GraphModel Deserialize(string serializedGraph);
    }
}