using System.Threading.Tasks;
using Hask.GraphVisualization.Core.DTO;
using Hask.GraphVisualization.WebApi.Helpers.GraphVIsualization;

namespace Web.Core.Helpers.GraphVIsualization
{
    public interface IGraphVisualizationHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        /// <returns>возвращает урл изображения графа</returns>
        Task<GraphImage> GenerateGraphImage(GraphModel graph);
    }
}