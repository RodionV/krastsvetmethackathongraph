namespace Hask.GraphVisualization.WebApi.Helpers.GraphVIsualization
{
    public class GraphImage
    {
        public string Url { get; set; }
    }
}