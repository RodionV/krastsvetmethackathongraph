using System;
using System.IO;
using System.Threading.Tasks;
using Hask.GraphVisualization.Core.DTO;
using Hask.GraphVisualization.GraphVizUtil;
using Hask.GraphVisualization.WebApi.Helpers.GraphVIsualization;
using Web.Core.Helpers.GraphSerializer;

namespace Web.Core.Helpers.GraphVIsualization
{
    public class GraphVisualizationHelper : IGraphVisualizationHelper
    {
        private readonly GraphVizUtil _graphVizUtil;
        private readonly DotGraphSerializer _dotGraphSerializer = 
            new DotGraphSerializer(new DotBuilder());

        private static string _baseInputPath =
            "C:\\Users\\galat\\Desktop\\hackaton\\krastsvetmethackathongraph\\Hask.GraphVisualization\\Web.Core\\wwwroot\\temp\\";

        private static string _baseOutputPath =
            "C:\\Users\\galat\\Desktop\\hackaton\\krastsvetmethackathongraph\\Hask.GraphVisualization\\Web.Core\\wwwroot\\img\\graphs\\";

        public GraphVisualizationHelper(GraphVizUtil graphVizUtil)
        {
            _graphVizUtil = graphVizUtil;
        }

        public async Task<GraphImage> GenerateGraphImage(GraphModel graph)
        {
            var fileName = Guid.NewGuid().ToString() + ".svg";
            var filePath = _baseOutputPath + fileName;
            var dotString = _dotGraphSerializer.Serialize(graph);
            using (var file =  File.Create(filePath))
            {
                using (var writer = new StreamWriter(file))
                {
                    await writer.WriteLineAsync(dotString);
                }
            }

            var generateOptions = new GenerateGraphOptions
            {
                Format = GraphVizOutputFormats.Svg,
                InputFilePath = filePath,
                OutputFilePath = _baseOutputPath + fileName
            };
            await _graphVizUtil.GenerateGraph(generateOptions);

            return new GraphImage
            {
                Url = $"/img/graphs/{fileName}"
            };
        }
    }
}