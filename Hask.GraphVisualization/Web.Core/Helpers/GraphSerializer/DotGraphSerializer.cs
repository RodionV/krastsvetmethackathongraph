using System;
using Hask.GraphVisualization.Core.DTO;

namespace Web.Core.Helpers.GraphSerializer
{
    public class DotGraphSerializer : IGraphSerializer
    {
        private readonly IDotBuilder _dotBuilder;

        public DotGraphSerializer(IDotBuilder dotBuilder)
        {
            _dotBuilder = dotBuilder;
        }

        public string Serialize(GraphModel graph)
        {
            _dotBuilder.AddEdges(graph.Edges);
            _dotBuilder.AddVertices(graph.Vertices);

            return _dotBuilder.Build();
        }
    }
}