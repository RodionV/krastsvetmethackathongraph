using System;
using System.Collections;
using System.Collections.Generic;
using Hask.GraphVisualization.Core.DTO;

namespace Web.Core.Helpers.GraphSerializer
{
    public class DotBuilder : IDotBuilder
    {
        private List<VertexModel> _vertices = new List<VertexModel>();
        private List<EdgeModel> _edges = new List<EdgeModel>();
        private string _serializedGraph = "";
        private static string _bodyStart = "digraph G { node [shape=rectangle]; ";
        private static string _bodyEnd = "}";
        
        
        public string Build()
        {
            _serializedGraph += _bodyStart;
            foreach (var vertix in _vertices)
            {
                _serializedGraph += $"n{vertix.Id}[label=\"{vertix.Text}\"]; "; //, fixedsize=\"true\", " +
                //$"width=\"{vertix.Width / 72}\", height=\"{vertix.Height / 72}\"]; ";
            }

            foreach (var edge in _edges)
            {
                _serializedGraph += $"n{edge.InVertexId}->n{edge.OutVertexId}; ";
            }

            _serializedGraph += _bodyEnd;

            return _serializedGraph;
        }
        
        public void AddVertices(IEnumerable<VertexModel> vertices)
        {
            _vertices.AddRange(vertices);
        }
        
        public void AddEdges(IEnumerable<EdgeModel> edges)
        {
            _edges.AddRange(edges);
        }
    }
}