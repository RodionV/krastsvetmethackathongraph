using Hask.GraphVisualization.Core.DTO;

namespace Web.Core.Helpers.GraphSerializer
{
    public interface IGraphSerializer
    {
        string Serialize(GraphModel graph);
    }
}