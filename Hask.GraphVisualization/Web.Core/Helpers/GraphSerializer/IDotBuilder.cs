using System.Collections.Generic;
using Hask.GraphVisualization.Core.DTO;

namespace Web.Core.Helpers.GraphSerializer
{
    public interface IDotBuilder
    {
        string Build();

        void AddVertices(IEnumerable<VertexModel> vertices);

        void AddEdges(IEnumerable<EdgeModel> edges);
    }
}