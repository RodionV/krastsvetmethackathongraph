using System.Diagnostics;
using System.Threading.Tasks;
using Hask.GraphVisualization.GraphVizUtil;

namespace Web.Core.Helpers.ExecuteUtil
{
    public class ExecuteUtil : IExecuteUtil
    {
        public Task Execute(string path, string args)
        {
            return Task.Run(() =>
            {
                using (var process = new Process())
                {
                    process.StartInfo.FileName = path;
                    process.StartInfo.Arguments = args;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.Start();
                    process.WaitForExit();
                }
            });
        }
    }
}