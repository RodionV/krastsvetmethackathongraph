$('#send-file').click(function () {
    let fd = new FormData();

    fd.append("file", $('#inputfile')[0].files[0]);
    // var request = new XMLHttpRequest();
    // request.open("POST", "graph/upload");
    // request.send(formData);
    $.ajax({
        url: 'graph/upload',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response != 0) {
                alert('file uploaded');
            } else {
                alert('file not uploaded');
            }
        },
    });
});