using System.IO;
using System.Threading.Tasks;
using Hask.GraphVisualization.Core.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Web.Core.Helpers.GraphDeserializer;
using Web.Core.Helpers.GraphSerializer;
using Web.Core.Helpers.GraphVIsualization;

namespace Web.Core.Controllers
{
    public class GraphController : Controller
    {
        private readonly IGraphVisualizationHelper _graphVisualizationHelper;

        public GraphController(IGraphVisualizationHelper graphVisualizationHelper)
        {
            _graphVisualizationHelper = graphVisualizationHelper;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile serializedGraphFile)
        {
            var serializedGraph = "";
            using (var fileStream = serializedGraphFile.OpenReadStream())
            {
                using (var reader = new StreamReader(fileStream))
                {
                    serializedGraph = reader.ReadToEnd();
                }
            }

            var graph = JsonConvert.DeserializeObject<GraphModel>(serializedGraph);

            var graphImage = await _graphVisualizationHelper.GenerateGraphImage(graph);

            return View(graphImage);
        }
    }
}