using System.Threading.Tasks;
using Hask.GraphVisualization.Core.DTO;

namespace Hask.GraphVisualization.Core.Algorighms
{
    public class FakeGraphOptimizationAlgorithm : IGraphOptimizationAlgorithm
    {
        public GraphModel Optimize(GraphModel oldGraph)
        {
            return oldGraph;
        }

        public Task<GraphModel> OptimizeAsync(GraphModel oldGraph)
        {
            return Task.FromResult(oldGraph);
        }
    }
}