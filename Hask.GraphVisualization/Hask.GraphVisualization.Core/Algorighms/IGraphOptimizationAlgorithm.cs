using System.Threading.Tasks;
using Hask.GraphVisualization.Core.DTO;

namespace Hask.GraphVisualization.Core.Algorighms
{
    public interface IGraphOptimizationAlgorithm
    {
        GraphModel Optimize(GraphModel oldGraph);

        Task<GraphModel> OptimizeAsync(GraphModel oldGraph);
    }
}