namespace Hask.GraphVisualization.Core.DTO
{
    public class EdgeModel
    {
        public int Id { get; set; }
        
        public int InVertexId { get; set; }
        
        public int OutVertexId { get; set; }
    }
}