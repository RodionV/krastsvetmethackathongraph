using System.Collections.Generic;

namespace Hask.GraphVisualization.Core.DTO
{
    public class VertexModel
    {
        public int Id { get; set; }
        
        public string Text { get; set; }
        
        public double Width { get; set; }
        
        public double Height { get; set; }
        
        public double X { get; set; }
        
        public double Y { get; set; }
        
        public bool IsItPossibleToChangeLocation { get; set; }
        
        public IEnumerable<int> InEdgeIds { get; set; }
        
        public IEnumerable<int> OutEdgeIds { get; set; }
    }
}