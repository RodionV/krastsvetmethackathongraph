using System.Collections.Generic;

namespace Hask.GraphVisualization.Core.DTO
{
    public class GraphModel
    {
        public IEnumerable<VertexModel> Vertices { get; set; }
        
        public IEnumerable<EdgeModel> Edges { get; set; }
    }
}