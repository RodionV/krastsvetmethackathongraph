using System.Collections.Generic;
using Hask.GraphVisualization.Core.DTO;
using Web.Core.Helpers.GraphSerializer;
using Xunit;

namespace Hask.GraphVisualization.Test.WebApi
{
    public class GraphSerializerTest
    {
        [Fact]
        public async void Test()
        {
            var edges = new List<EdgeModel>
            {
                new EdgeModel
                {
                    Id = 1,
                    InVertexId = 1,
                    OutVertexId = 2
                },
                new EdgeModel
                {
                    Id = 2,
                    InVertexId = 2,
                    OutVertexId = 3
                },
                new EdgeModel
                {
                    Id = 3,
                    InVertexId = 1,
                    OutVertexId = 3
                },
            };

            var vertecies = new List<VertexModel>
            {
                new VertexModel
                {
                    Height = 160,
                    Width = 160,
                    Id = 1,
                    Text = "1"
                },
                new VertexModel
                {
                    Height = 160,
                    Width = 160,
                    Id = 2,
                    Text = "2"
                },
                new VertexModel
                {
                    Height = 160,
                    Width = 160,
                    Id = 3,
                    Text = "3"
                },
            };

            var dotBuider = new DotBuilder();
            dotBuider.AddEdges(edges);
            dotBuider.AddVertices(vertecies);
            var result = dotBuider.Build();

        }
    }
}