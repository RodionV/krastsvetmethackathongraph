using Web.Core.Helpers.ExecuteUtil;
using Xunit;


namespace Hask.GraphVisualization.Test.WebApi
{
    public class ExecuteUtilTest
    {
        [Fact]
        public async void Test()
        {
            var execUtil = new ExecuteUtil();
            var args =
                " -spoint  C:\\Users\\galat\\Desktop\\hackaton\\Test\\test.gv -o C:\\Users\\galat\\Desktop\\hackaton\\Test\\testoutput.ps";
            var path = $"C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            await execUtil.Execute(path, args);
        }
    }
}