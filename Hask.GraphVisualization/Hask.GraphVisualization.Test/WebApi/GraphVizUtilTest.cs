using System.Threading.Tasks;
using Hask.GraphVisualization.GraphVizUtil;
using Web.Core.Helpers.ExecuteUtil;
using Xunit;

namespace Hask.GraphVisualization.Test.WebApi
{
    public class GraphVizUtilTest
    {
        private static GraphVizOptions _options = new GraphVizOptions
        {
            PathToUtil = $"C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe"
        };
        
        [Fact]
        public async void Test()
        {
            var graphVizUtil = new GraphVizUtil.GraphVizUtil(_options, new ExecuteUtil());
            var generateOptions = new GenerateGraphOptions
            {
                InputFilePath = "C:\\Users\\galat\\Desktop\\hackaton\\Test\\test.gv",
                OutputFilePath = "C:\\Users\\galat\\Desktop\\hackaton\\Test\\testoutput.svg",
                Format = GraphVizOutputFormats.Svg
            };
            await graphVizUtil.GenerateGraph(generateOptions);
        }
    }
}